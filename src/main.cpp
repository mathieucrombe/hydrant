#include <Arduino.h>        //Pour accéder à la bibliothèque Arduino
#include <Wire.h>           //pour accéder au bus I2C
#include "SparkFunBME280.h" //inclusion de la lib BMP280
#include <SoftwareSerial.h> //Pour affécter des ports au Rx/Tx et ainsi utiliser le UART (Universal Asyncronous Receiver Transmiter)
#include <SparkFun_ADXL345.h>

#define us_to_s_factore 1000000   /*Pour convertir les secondes vers des micros seconde*/
#define time_to_sleep 1800        /*Definis le temps pour dormir*/
#define txPin 4                   //sérigraphie D2
#define rxPin 5                   //sérigraphie D1
#define BUTTON_PIN_BITMASK 0x4000 // GPIOs 14

RTC_DATA_ATTR int veilleeveil = 0;

BME280 capteurBMP280;     //pour communiquer avec le capteur
int sdaBMP280 = 13;       //broche SDA BMP280
int sclBMP280 = 15;       //broche SCL BMP280
int addrI2CBMP280 = 0x77; //adresses I2C BMP280

float pression = 0.0; //pour stocker la pression
float valtemp = 0.0;  //pour stocker la température
int temp = 0;         //pour les conversion en hexa

// Création d'un objet de la lib ADXL345
// pour accéléromètre connecté en I2C
 ADXL345 adxl = ADXL345();
 int x, y, z; //valeurs d'accélération sur les 3 axes
 float xg, yg, zg;
 float soh;
 float sah;
 float tilt;
 float tilt2;
 float angle;
 String dir;

unsigned long octetH = 0;  //Contiendra l'octet Haut
unsigned char octetHH = 0; //contiendra les bout de l'octet Haut en hexa
unsigned char octetB = 0;  //contiendra l'octet Bas
unsigned char octetBH = 0; //contiendra les bout de l'octet Bas en hexa
unsigned char cara[12];    //contiendra les valeur hexa séparé
char caraH[24];            //contiendra l'équivalent asci des valeur hexa             2147483648
int indexcara = 0;         //2147483648

String msgConsole = ""; //message pour la console
int etat = 0;           //creation de la valeur d'etat
int state = 0;
int chekup=0;
char incomingChar;
String test = "AT$SF="; //Valeur par défault de la tram envoyé via sigfox
String tram="AT$SF=000003000000";

void print_wakeup_reason();
void print_GPIO_wake_up();

//création d'un objet SoftwareSerial

SoftwareSerial swSer1(rxPin, txPin);

void setup()
{
  Serial.begin(115200);
  swSer1.begin(9600, SWSERIAL_8N1);           //Conf port Rx/Tx

  //Print the wakeup reason for ESP32
  print_wakeup_reason();

  //Reveille le processeur en appuyant sur un bouton connecte au GPIO2

  Serial.println("Etat de l'hydrant = " + String(veilleeveil));

  if (state == 1){if (veilleeveil == 0){veilleeveil = 1;}else{veilleeveil = 0;}}

  esp_sleep_enable_ext0_wakeup(GPIO_NUM_2, 1);

   if (veilleeveil == 1)
  {
  
  //Initialisation des différent port

  Wire.begin(sdaBMP280, sclBMP280);           //Conf de la liaison I2C
  capteurBMP280.setI2CAddress(addrI2CBMP280); //adresse I2C du BMP280


  if (capteurBMP280.beginI2C(Wire) == false)
  {
    Serial.println("BMP280 : communication impossible");
  }

  adxl.powerOn();
  adxl.setRangeSetting(16); // gamme de mesure jusqu'à 16G
  adxl.setFullResBit(true); //résolution sur 13 bits (Scale Factor = 4mg/LSB)
  adxl.setRate(3200);       // data rate = 3200Hz

  //Assignation des Valeurs
   for (int i = 0; i < 12; i++)
  {
    cara[i] = 0;
  }
  //Valeur pour l'etat

  //Initialisation de la valeur d'etat
  
    print_GPIO_wake_up();

  //Préparation pour le changement de la valeur d'etat



  cara[5] = etat;

  //Lecture température

  Serial.print(" Temp: ");                      //affichage d'un message en mod console
  Serial.println(capteurBMP280.readTempC(), 2); //affichage de la temp en mod console
  valtemp = capteurBMP280.readTempC(), 2;

  //Conversion des valeurs

  if (valtemp < 0)
  {
    valtemp = valtemp * -1;
    cara[0] = 0xF;
  }

  temp = valtemp * 10;     //multipliqation de la valeur pour récupérer la valeur au dixième prés
  octetH = temp & 0xFFF;   //masque pour récupérer octet Haut
  octetH = octetH >> 8;    //Décallage de l'octet Haut pour le faire passer en Bas
  octetHH = octetH & 0xF0; //masque pour récupérer octet Haut de l'octet Haut
  octetHH = octetHH >> 4;  //Décallage de l'octet Haut de l'octet Haut en décallage
  cara[1] = octetHH;       //insertion de l'octet haut de l'octet haut dans le vecteur
  octetHH = octetH & 0x0F; //Récupération de l'octet bas de l'octet Haut
  cara[2] = octetHH;       //insertion de l'octet bas de l'octet haut dans le vecteur
  octetB = temp & 0x00FF;  //masque pour récupérer octet Bas
  octetBH = octetB & 0xF0; //masque pour récupérer octet Haut de l'octet Bas
  octetBH = octetBH >> 4;  //Décallage de l'octet Haut de l'octet Bas
  cara[3] = octetBH;       //insertion de l'octet haut de l'octet Bas dans le vecteur
  octetBH = octetB & 0x0F; //Récupération de l'octet bas de l'octet Bas
  cara[4] = octetBH;       //insertion de l'octet Bas de l'octet Bas dans le vecteur

  //Lecture de l'inclinaison

   int x, y, z;                // init variables hold results
  adxl.readAccel(&x, &y, &z); // Read the accelerometer values and store in variables x,y,z
  Serial.print(x);
  Serial.print(", ");
  Serial.print(y);
  Serial.print(", ");
  Serial.println(z);

  xg = x * 0.0039;
  yg = y * 0.0039;
  zg = z * 0.0039;

  soh = yg / zg;
  sah = xg / zg;

  tilt = atan(soh) * 57.296;

  if (soh >= sah)
  {tilt = atan(soh) * 57.296;}else{tilt = atan(sah) * 57.296;}
  
  if (abs(tilt) > 90)
  {

    Serial.print("Tilt:Range Error");
  }
  else
  {

    if (tilt < 0)
    {
      dir = "Up";
      angle = abs(tilt);
    }
    else
    {
      dir = "Down";
      angle = tilt;
    }
    if ((angle <= 1.94))
    {
      tilt = 0;
    }
    else
    {
      angle = angle - 1.94;
    }
    Serial.print("Tilt:");
    Serial.print(angle);
    Serial.write(byte(0));
    Serial.println(dir);
  }

    temp = angle * 100;     //multipliqation de la valeur pour récupérer la valeur au dixième prés
  octetH = temp & 0xFFF;   //masque pour récupérer octet Haut
  octetH = octetH >> 8;    //Décallage de l'octet Haut pour le faire passer en Bas
  octetHH = octetH & 0xF0; //masque pour récupérer octet Haut de l'octet Haut
  octetHH = octetHH >> 4;  //Décallage de l'octet Haut de l'octet Haut en décallage
  cara[6] = octetHH;       //insertion de l'octet haut de l'octet haut dans le vecteur
  octetHH = octetH & 0x0F; //Récupération de l'octet bas de l'octet Haut
  cara[7] = octetHH;       //insertion de l'octet bas de l'octet haut dans le vecteur
  octetB = temp & 0x00FF;  //masque pour récupérer octet Bas
  octetBH = octetB & 0xF0; //masque pour récupérer octet Haut de l'octet Bas
  octetBH = octetBH >> 4;  //Décallage de l'octet Haut de l'octet Bas
  cara[8] = octetBH;       //insertion de l'octet haut de l'octet Bas dans le vecteur
  octetBH = octetB & 0x0F; //Récupération de l'octet bas de l'octet Bas
  cara[9] = octetBH;       //insertion de l'octet Bas de l'octet Bas dans le vecteur

  for (int i = 0; i < 12; i++) //passage des valeur du vecteur en leur version ascii
  {
    if (cara[i] <= 9)
    {
      cara[i] = cara[i] + 0x30;
    }
    else
    {
      cara[i] = cara[i] + 55;
    }
  }
  Serial.print("La tram à envoyer = ");
  for (int i = 0; i < 12; i++) //passage des valeur du vecteur en leur version ascii
  {
    caraH[i] = cara[i];
    Serial.print(caraH[i]);
  }
  Serial.println(" ");

  //Utilisation du Modem Sigfox

  Serial.println((String) "Recopie le moniteur série de PlatformIO sur Rx=" + rxPin + " et Tx=" + txPin); //affichage d'un message

  //Envoie du message sigfox ici etant ma tram par default 000000000000
  for (int i = 0; i < 12; i++)
  {
    test = test + caraH[i];
  }

  swSer1.println(test + '\r');

  incomingChar = (char)swSer1.read();
  Serial.print(incomingChar);

  //affichage de letat d'ouverture en mod console
  Serial.print("L'etat = " + String(etat));
  Serial.println();

  //Reglage pour le mod deep sleep de l'esp

  esp_sleep_enable_timer_wakeup(time_to_sleep * us_to_s_factore);                                        //Setup du temps de sommeil de l'esp ici 30mn
  esp_sleep_enable_ext1_wakeup(BUTTON_PIN_BITMASK, ESP_EXT1_WAKEUP_ANY_HIGH);
  Serial.println("Mise en place de l'esp32 pour dormir tout les " + String(time_to_sleep) + " Seconde"); //affichage d'un message en mod console
  delay(1000);                                                                                           //Setup un delay pour le lui laisser le temps d'éxécuter ses taches
  Serial.flush();                                                                                        //Vide le buffer de réception de données du port série (vide le buffer de donnée)
  esp_deep_sleep_start();                                                                                //lancement du deepsleep (a son reveill le programme recommencera depuis le debut)
  }

  // Envoie d'un message sigfox

  swSer1.println(tram + '\r');

  incomingChar = (char)swSer1.read();
  Serial.print(incomingChar);

  //Go to sleep now
  Serial.println("Maintenant tu dors");                                                                  //affichage d'un message en mod console
  delay(1000);                                                                                           //Setup un delay pour le lui laisser le temps d'éxécuter ses taches
  Serial.flush();                                                                                        //Vide le buffer de réception de données du port série (vide le buffer de donnée)
  esp_deep_sleep_start();                                                                                //lancement du deepsleep (a son reveill le programme recommencera depuis le debut)
  Serial.println("Toi tu t'affiche pas");                                                                //test pour vérifier si le deepsleep fonctionne bien

}

void loop()
{
}

void print_wakeup_reason()
{
  esp_sleep_wakeup_cause_t wakeup_reason;
  wakeup_reason = esp_sleep_get_wakeup_cause();
  switch (wakeup_reason)
  {
  case 2:
    state = 1;
    break;
  }
}

void print_GPIO_wake_up()
{
  uint64_t GPIO_reason = esp_sleep_get_ext1_wakeup_status();
  Serial.print("GPIO that triggered the wake up: GPIO ");
  etat = (log(GPIO_reason)) / log(2);
  if (etat != 14)
  {
    Serial.println("none");
    etat = 0;
  }
  if (etat == 14)
  {
    Serial.println(etat);
    etat = 1;
  }

  ;
}
